## 0.6.1
- Update bpriver_origin library
    - 0.6.1
- Update bpriver_debug library
    - 0.6.1

## 0.6.0
- Update bpriver_origin library
    - 0.6.0
- Update bpriver_debug library
    - 0.6.0

## 0.5.2
- Update bpriver origin
    - 0.5.2
- Update bpriver debug
    - 0.5.3

## 0.5.1
- Update bpriver origin
    - 0.5.1
- Update bpriver debug
    - 0.5.2

## 0.5.0
- Update bpriver origin
    - 0.5.0
- Update bpriver debug
    - 0.5.0

## 0.4.2
- Update bpriver origin
    - 0.4.2
- Update bpriver debug
    - 0.4.4

## 0.4.1
- Update bpriver origin
    - 0.4.1
- Update bpriver debug
    - 0.4.1

## 0.4.0
- Update bpriver origin
    - 0.4.0
- Update bpriver debug
    - 0.4.0

## 0.3.8
- Update bpriver origin
    - 0.3.4
- Update bpriver debug
    - 0.3.4

## 0.3.7
- Update bpriver origin
    - 0.3.3
- Update bpriver debug
    - 0.3.3

## 0.3.6
- Edit SafetyStringScanner.lookWhileUntils
    - look index を until の次まで進める.

## 0.3.5
- Edit SafetyStringScanner.lookWhileUntils
    - escape sequence 機能を取り入れる.

## 0.3.3
- Add SafetyStringScanner.lookWhileUntils

## 0.3.2
- Add SafetyStringScanner.fromString
- Add SafetyStringScanner.lookWhileCharacters

## 0.3.1
- Update dart sdk
    - 3.6.0

## 1.0.0

- Initial version