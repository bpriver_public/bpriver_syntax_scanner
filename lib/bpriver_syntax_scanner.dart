// Copyright (C) 2023, the bpriver_syntax_scanner project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

library bpriver_syntax_scanner;

import 'package:bpriver_origin/bpriver_origin.dart';

part 'src/syntax_scanner/syntax_scanner.dart';

part 'src/syntax_scanner/safety/safety_scanner.dart';
part 'src/syntax_scanner/safety/object/safety_object_scanner.dart';
part 'src/syntax_scanner/safety/syntax/safety_syntax_scanner.dart';
part 'src/syntax_scanner/safety/string/safety_string_scanner.dart';

part 'src/bpriver_syntax_scanner_exception/bpriver_syntax_scanner_exception.dart';
part 'src/bpriver_syntax_scanner_error/bpriver_syntax_scanner_error.dart';
