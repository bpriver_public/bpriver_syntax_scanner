// Copyright (C) 2023, the bpriver_syntax_scanner project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.
 
part of 'package:bpriver_syntax_scanner/bpriver_syntax_scanner.dart';

// TODO test ように aggregation pattern は with する.
// TODO test ように 任意の property を設定できる用の constructor を用意する.

// ST extends OT にするならば
// A. scanner 内部で ko を otsu に移す
// が可能.
// しかし 代わりに String を ko として持つことができなくなる. つまり 最初の source を解析する処理ができなくなる.
// ST extends Object なら source の解析も可能だが A ができなくなる.
// 今のところどちらを選ぶかになっている.

// 実行速度重視ならば index などのチェックは最小限、もしくは、呼び出し元で管理させるべきでは?
//     Safety のように 何かしら error を throw しないように処理をすることを求める型を定義すべき.
//     また 速度重視の method と 安全重視の method の両方を実装するか?
//          速度重視 or 安全重視 の 2つの実装 class を用意する?
//          これなら 同じ method 名を使え Result<V> で汎化もできる.

// syntax scanner には data を保持させない.
// data の保持は 呼び出し側で行わせる.
// data の保持をしてしまうと どうしても function を受け取るような処理になってしまい 処理速度も複雑さも悪化する.
// あくまで作業場所としての一時的な data の保持しかさせない.

/// {@template SyntaxScanner}
/// 
/// ST...sourtce type
/// OT...outcome type
/// 
/// 実行速度を重視したいため mutable class として実装.<br>
/// 
/// {@endtemplate}
sealed class SyntaxScanner<
        ST extends Object
        ,OT extends Object
    >
    with
        AggregationPattern
{

    /// {@template SyntaxScanner._source}
    /// _source. 解析対象となる syntax を保持.
    /// source として 文字列の集まりを渡したい場合は character の list として渡す.
    /// {@endtemplate}
    Iterable<ST> get _source; 

    // add 操作をするため List.
    /// {@template SyntaxScanner._outcome}
    /// 乙. 解析後の syntax を保持.
    /// {@endtemplate}
    List<OT> get _outcome;

    int get _scanIndex;
    void set _scanIndex(int value);

    int get _lookIndex;
    void set _lookIndex(int value);

    static int _getEndIndex(Iterable<Object> _source) => _source.length - 1;

    const SyntaxScanner();

    List<ST> get source => List.generate(_source.length, (index) => _source.elementAt(index));
    List<OT> get outcome => List.generate(_outcome.length, (index) => _outcome.elementAt(index));
    
    int get scanIndex => _scanIndex;
        
    int get lookIndex => _lookIndex; 

    int get endIndex; // _source の length - 1 を保持する.

    @override
    Map<String, Object> get properties {
        return {
            '_source': _source,
            '_outcome': _outcome,
            '_scanIndex': _scanIndex,
            '_lookIndex': _lookIndex,
        };
    }

    // debug 用
    List<Type> get sourceTypes => List.generate(_source.length, (index) => _source.elementAt(index).runtimeType);

    // debug 用
    List<Type> get outComeTypes => List.generate(_outcome.length, (index) => _outcome.elementAt(index).runtimeType);

    /// {@template SyntaxScanner.overScanIndex}
    /// [_scanIndex] が [endIndex] より大きくなった場合 true.
    /// {@endtemplate}
    bool get overScanIndex => _scanIndex > endIndex;

    /// {@template SyntaxScanner.overLookIndex}
    /// [_lookIndex] が [endIndex] より大きくなった場合 true.
    /// {@endtemplate}
    bool get overLookIndex => _lookIndex > endIndex;

    /// {@template SyntaxScanner.look}
    /// 現在の [_lookIndex] の位置にある 要素 を返す.
    /// 要素 が存在しない場合 失敗する.
    /// [_lookIndex] に 1つ進める.
    /// {@endtemplate}
    Result<ST> look();

    /// {@template SyntaxScanner.scan}
    /// 成功すると下記の処理を実行する.
    /// 1. [_outcome] に [otsu] を追加する.
    /// 2. [_scanIndex] を [_lookIndex] の位置まで進める.
    /// {@endtemplate}
    Safety<Complete> scan(OT otsu) {

        final log = Log(classLocation: runtimeType, functionLocation: 'scan');

        _outcome.add(otsu);

        _scanIndex = _lookIndex;

        return Safety(Complete(), log);

    }

    /// {@template SyntaxScanner.lookWhile}
    /// [T] 型 が続く限り 取得し続け List<T> で返す.
    /// [T] が続いた分だけ [_lookIndex] を加算する.
    /// {@endtemplate}
    Safety<List<T>> lookWhile<T extends ST>() {

        const log = Log.const$(SafetySyntaxScanner, 'lookWhile');

        final List<T> list = [];

        while (true) {

            // null が返ったなら overIndex ということ.
            // null の場合も T ではないので 処理が終了する.
            final currentSyntax = _source.elementAtOrNull(_lookIndex);
            final isResult = currentSyntax is T;

            if (!isResult) break;
            
            list.add(currentSyntax);
            _lookIndex = _lookIndex + 1;

        }

        return Safety.const$(list, log);

    }

    /// {@template SyntaxScanner.reset}
    /// 以下の処理を実行する.
    /// 1. [_lookIndex] の位置を [_scanIndex] の位置に合わせる.
    /// {@endtemplate}
    Safety<Complete> reset();

    /// {@template SyntaxScanner.shift}
    /// 指定した数だけ scan index を移動する.
    /// 残りの syntax が存在しない場合 何もしない.
    /// nubmer には 負の値 もしくは 0 が指定されることは想定していない.
    ///     想定し それを確認するための分岐処理を持たせると overhead になるため(実行速度を重視).
    ///     大きすぎる負の値を指定し scanIndex が 負の値になった場合 index error が発生(たぶん).
    /// number の値が大きすぎても すべて overScanIndex として処理されるため特に問題ない
    /// {@endtemplate}
    void shift([int number = 1]) {

        if (overScanIndex) return;

        _scanIndex = _scanIndex + number;

    }

    /// {@template SyntaxScanner.shiftAtLookIndex}
    /// [_lookIndex] の位置まで [_scanIndex] を移動する.
    /// {@endtemplate}
    Safety<Complete> shiftAtLookIndex() {

        const log = Log.const$(SyntaxScanner, 'shiftAtLookIndex');

        _scanIndex = _lookIndex;

        return const Safety.const$(Complete(), log);

    }

}
