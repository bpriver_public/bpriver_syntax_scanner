// Copyright (C) 2023, the bpriver_syntax_scanner project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.
 
part of 'package:bpriver_syntax_scanner/bpriver_syntax_scanner.dart';

/// {@template SafetyObjectScanner}
/// 
/// {@endtemplate}
final class SafetyObjectScanner<
        ST extends Object
        ,OT extends Object
    >
    extends
        SafetyScanner<ST, OT>
{

    final Iterable<ST> _source; 
    final List<OT> _outcome = [];
    final int endIndex;
    int _scanIndex = 0;
    int _lookIndex = 0;

    SafetyObjectScanner(this._source)
    :
        endIndex = SyntaxScanner._getEndIndex(_source)
    ;

}
