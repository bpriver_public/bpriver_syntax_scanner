// Copyright (C) 2023, the bpriver_syntax_scanner project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.
 
part of 'package:bpriver_syntax_scanner/bpriver_syntax_scanner.dart';

/// {@template SafetyStringScanner}
/// 
/// {@endtemplate}
final class SafetyStringScanner<
        OT extends Object
    >
    extends
        SafetyScanner<String, OT>
{

    final Iterable<String> _source; 
    final List<OT> _outcome = [];
    final int endIndex;
    int _scanIndex = 0;
    int _lookIndex = 0;

    SafetyStringScanner(this._source)
    :
        endIndex = SyntaxScanner._getEndIndex(_source)
    ;

    factory SafetyStringScanner.fromString(String source) {

        final splitResult = source.split('');

        final result = SafetyStringScanner<OT>(splitResult);

        return result;

    }

    // 速度の観点から syntax scanner のように compile は実装しない.
    //  分岐が多いので.

    /// {@template SyntaxScanner.lookWhileCharacters}
    /// [characters] が続く限り 取得し続け List<String> で返す.<br>
    /// [characters] が続いた分だけ [_lookIndex] を加算する.<br>
    /// {@endtemplate}
    Safety<List<String>> lookWhileCharacters(Iterable<String> characters) {

        const log = Log.const$(SafetyStringScanner, 'lookWhileCharacters');

        final List<String> list = [];

        while (true) {

            final currentCharacter = _source.elementAtOrNull(_lookIndex);
            if (currentCharacter == null) break;
            final isResult = characters.contains(currentCharacter);

            if (!isResult) break;
            
            list.add(currentCharacter);
            _lookIndex = _lookIndex + 1;

        }

        return Safety.const$(list, log);

    }

    /// {@template SyntaxScanner.lookWhileUntils}
    /// [untils] のいずれかが出現するまで 取得し続け, その出現した [untils] を除く List<String> で返す.<br>
    /// [untils] のいずれかの出現場所の次まで [_lookIndex] を加算する.<br>
    /// 最後まで [untils] のいずれも出現しなかった場合 [BpriverSyntaxScannerExceptionD] を返す.<br>
    /// [untils] のそれぞれの要素の長さが 1 ではない場合 thorow [Panic<BpriverSyntaxScannerErrorC>].<br>
    /// {@endtemplate}
    Disaster<List<String>, BpriverSyntaxScannerExceptionD, BpriverSyntaxScannerErrorC> lookWhileUntils(Iterable<String> untils, [String escapeSequence = '\\']) {

        final log = Log(classLocation: runtimeType, functionLocation: 'lookWhileUntils');

        for (final i in untils) {
            if (i.length != 1) Result.panic(BpriverSyntaxScannerErrorC(), log.monitor({
                'error': i,
                'untils': untils,
            }));
        }

        final List<String> list = [];

        while (true) {

            final currentCharacter = _source.elementAtOrNull(_lookIndex);
            if (currentCharacter == null) return Suffer(BpriverSyntaxScannerExceptionD(), log.monitor({
                'list': list,
            }));

            final isResult = untils.contains(currentCharacter);

            if (isResult) {

                final escapeSequenceLength = escapeSequence.length;
                final listLength = list.length;
                // この場合 escape sequence が含まれることはありえない.
                if (listLength < escapeSequenceLength) {
                    _lookIndex = _lookIndex + 1;
                    break;
                }

                // 上記より list length は必ず escape sequence length 以上になる.
                final skipLength = listLength - escapeSequenceLength;
                final endResult = list.skip(skipLength).join();
                // 末尾が escape sequence じゃない場合 break. 
                if (endResult == escapeSequence) {
                    // escape sequence を list から取り除く.
                    list.removeRange(listLength - escapeSequenceLength, listLength);
                } else {
                    _lookIndex = _lookIndex + 1;
                    break;
                }

            }
            
            list.add(currentCharacter);
            _lookIndex = _lookIndex + 1;

        }

        return Survive(list, log);

    }

}
