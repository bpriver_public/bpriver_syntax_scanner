// Copyright (C) 2023, the bpriver_syntax_scanner project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.
 
part of 'package:bpriver_syntax_scanner/bpriver_syntax_scanner.dart';

/// {@template SafetySyntaxScanner}
/// 
/// {@endtemplate}
final class SafetySyntaxScanner<
        ST extends OT
        ,OT extends Object
    >
    extends
        SafetyScanner<ST, OT>
{

    final Iterable<ST> _source; 
    final List<OT> _outcome = [];
    final int endIndex;
    int _scanIndex = 0;
    int _lookIndex = 0;

    SafetySyntaxScanner(this._source)
    :
        endIndex = SyntaxScanner._getEndIndex(_source)
    ;

    Safety<List<OT>> compile(
        Iterable<Safety<bool> Function(SafetySyntaxScanner<ST, OT>)> verifyActions,
        // T Function(Iterable<OT>) otsuConstructor,
    ) {

        final log = Log(classLocation: SafetySyntaxScanner, functionLocation: 'compile');

        while (true) {

            bool flag = false;

            for (final verifyAction in verifyActions) {
                final result = verifyAction(this);
                log.add(result);
                if (result.wrapped) {
                    flag = true;
                    break;
                }
            }

            if (flag) continue;

            final lookResult = look();
            log.add(lookResult);
            if (lookResult is! Success<ST, BpriverSyntaxScannerExceptionC>) break;

            final scanResult = scan(lookResult.wrapped);
            log.add(scanResult);

        }

        // final result = otsuConstructor(outcome);
        // log.add(result);

        // return Result.fromResult(result) as T;
        
        return Safety(outcome, log);

    }

}
