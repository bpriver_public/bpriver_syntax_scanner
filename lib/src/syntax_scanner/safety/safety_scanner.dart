// Copyright (C) 2023, the bpriver_syntax_scanner project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.
 
part of 'package:bpriver_syntax_scanner/bpriver_syntax_scanner.dart';

/// {@template SafetyScanner}
/// 
/// {@endtemplate}
sealed class SafetyScanner<
        ST extends Object
        ,OT extends Object
    >
    extends
        SyntaxScanner<ST, OT>
{

    SafetyScanner();

    @override
    Danger<ST, BpriverSyntaxScannerExceptionC> look() {

        final log = Log(classLocation: runtimeType, functionLocation: 'look');

        if (overLookIndex) return Failure(BpriverSyntaxScannerExceptionC(), log.monitor({
            'look index': _lookIndex,
            'scan index': _scanIndex,
            'end index': endIndex,
        }));

        final currentSyntax = _source.elementAt(_lookIndex);

        _lookIndex = _lookIndex + 1;

        // log.addMonitor('looked', currentSyntax.runtimeType.toString());

        return Success(currentSyntax, log);

    }

    @override
    Safety<Complete> reset() {

        final log = Log(classLocation: runtimeType, functionLocation: 'reset');

        _lookIndex = _scanIndex;

        // log.addMonitorAll({
        //     'look index': _lookIndex,
        //     'scan index': _scanIndex,
        // });

        return Safety(Complete(), log);

    }

}
