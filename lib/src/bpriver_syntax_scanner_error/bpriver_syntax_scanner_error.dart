// Copyright (C) 2023, the bpriver_syntax_scanner project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_syntax_scanner/bpriver_syntax_scanner.dart';

/// {@template BpriverSyntaxScannerError}
/// {@endtemplate}
sealed class BpriverSyntaxScannerError
    extends
        Error
    with
        AggregationPattern
    implements
        LoggerResultMessageSignature
{

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage,
        };
    }

}

/// {@template BpriverSyntaxScannerErrorA}
/// reached never process.
/// {@endtemplate}
final class BpriverSyntaxScannerErrorA
    extends
        BpriverSyntaxScannerError
{

    @override
    final List<String> loggerResultMessage;

    /// {@macro BpriverSyntaxScannerErrorA}
    BpriverSyntaxScannerErrorA()
    :
        loggerResultMessage = [
            'reached never process.',
        ]
    ;

}

/// {@template BpriverSyntaxScannerErrorB}
/// scan staging の実行に失敗.
/// {@endtemplate}
final class BpriverSyntaxScannerErrorB
    extends
        BpriverSyntaxScannerError
{

    @override
    final List<String> loggerResultMessage = const [
        'scan staging の実行に失敗.',
    ];

    /// {@macro BpriverSyntaxScannerErrorB}
    BpriverSyntaxScannerErrorB();

}

/// {@template BpriverSyntaxScannerErrorC}
/// [SafetyStringScanner.lookWhileUntils]`s untils argument each element length must be one.<br>
/// {@endtemplate}
final class BpriverSyntaxScannerErrorC
    extends
        BpriverSyntaxScannerError
{

    static const LOGGER_RESULT_MESSAGE = [
        '[SafetyStringScanner.lookWhileUntils]`s untils argument each element length must be one.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverSyntaxScannerErrorC}
    BpriverSyntaxScannerErrorC();

}