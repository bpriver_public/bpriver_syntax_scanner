// Copyright (C) 2023, the bpriver_syntax_scanner project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_syntax_scanner/bpriver_syntax_scanner.dart';

/// {@template BpriverSyntaxScannerException}
/// {@endtemplate}
/// * [BpriverSyntaxScannerExceptionA]
/// {@macro BpriverSyntaxScannerExceptionA}
/// * [BpriverSyntaxScannerExceptionB]
/// {@macro BpriverSyntaxScannerExceptionB}
/// * [BpriverSyntaxScannerExceptionC]
/// {@macro BpriverSyntaxScannerExceptionC}
/// * [BpriverSyntaxScannerExceptionD]
/// {@macro BpriverSyntaxScannerExceptionD}
/// * [BpriverSyntaxScannerExceptionE]
/// {@macro BpriverSyntaxScannerExceptionE}
/// * [BpriverSyntaxScannerExceptionF]
/// {@macro BpriverSyntaxScannerExceptionF}
/// * [BpriverSyntaxScannerExceptionG]
/// {@macro BpriverSyntaxScannerExceptionG}
/// * [BpriverSyntaxScannerExceptionH]
/// {@macro BpriverSyntaxScannerExceptionH}
/// * [BpriverSyntaxScannerExceptionI]
/// {@macro BpriverSyntaxScannerExceptionI}
/// * [BpriverSyntaxScannerExceptionJ]
/// {@macro BpriverSyntaxScannerExceptionJ}
/// * [BpriverSyntaxScannerExceptionK]
/// {@macro BpriverSyntaxScannerExceptionK}
/// * [BpriverSyntaxScannerExceptionL]
/// {@macro BpriverSyntaxScannerExceptionL}
sealed class BpriverSyntaxScannerException
    with
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    const BpriverSyntaxScannerException();

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage,
        };
    }

}

/// {@template BpriverSyntaxScannerExceptionA}
/// Failed to create syntax instance.
/// {@endtemplate}
class BpriverSyntaxScannerExceptionA
    extends
        BpriverSyntaxScannerException
{

    @override
    final List<String> loggerResultMessage;

    static List<String> generateMessage(dynamic arg) {
        
        final beginning = [
            'Failed to create syntax instance.',
            '${arg.runtimeType}',
        ];

        final exception = arg;

        switch (exception) {
            case LoggerResultMessageSignature():
                exception;
                return [
                    ...beginning,
                    ...exception.loggerResultMessage,
                ];
            case _:
                return [
                    ...beginning,
                    exception.toString(),
                ];
        }
    }

    BpriverSyntaxScannerExceptionA(Exception exception)
    :
        loggerResultMessage = generateMessage(exception)
    ;

}

/// {@template BpriverSyntaxScannerExceptionB}
/// scan index が end index を超過.
/// {@endtemplate}
final class BpriverSyntaxScannerExceptionB
    extends
        BpriverSyntaxScannerException
{

    @override
    final List<String> loggerResultMessage = const [
        'scan index が end index を超過.',
    ];

    /// {@macro BpriverSyntaxScannerExceptionB}
    BpriverSyntaxScannerExceptionB();

}

/// {@template BpriverSyntaxScannerExceptionC}
/// look index が end index を超過.
/// {@endtemplate}
final class BpriverSyntaxScannerExceptionC
    extends
        BpriverSyntaxScannerException
{

    @override
    final List<String> loggerResultMessage = const [
        'look index が end index を超過.',
    ];

    /// {@macro BpriverSyntaxScannerExceptionC}
    BpriverSyntaxScannerExceptionC();

}

/// {@template BpriverSyntaxScannerExceptionD}
/// 最後まで走査したが untils が見つからなかった.<br>
/// {@endtemplate}
final class BpriverSyntaxScannerExceptionD
    extends
        BpriverSyntaxScannerException
{

    @override
    final List<String> loggerResultMessage = const [
        '最後まで走査したが untils が見つからなかった.',
    ];

    /// {@macro BpriverSyntaxScannerExceptionD}
    BpriverSyntaxScannerExceptionD();

}
