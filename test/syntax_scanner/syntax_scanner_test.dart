// Copyright (C) 2023, the bpriver_syntax_scanner project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_syntax_scanner/bpriver_syntax_scanner.dart';
import 'package:test/test.dart';

void main() {
    
    // group('generative constructor', () {
    //     group('expected: return SyntaxScanner. ', () {
    //         final source = LeafSyntaxList([
    //             MeaningfulLeafSyntax.fromType<LowercaseAlphabetA>(SyntaxPosition(1)).wrapped,
    //             MeaningfulLeafSyntax.fromType<LowercaseAlphabetB>(SyntaxPosition(2)).wrapped,
    //             MeaningfulLeafSyntax.fromType<LowercaseAlphabetC>(SyntaxPosition(3)).wrapped,
    //         ]);
    //         final base = SyntaxScanner(source, <SecondLayerOrBelowSyntax>[]);
    //         final actual = base;
    //         test('', () {
    //             final expected = isA<SyntaxScanner>();
    //             expect(actual, expected);
    //         });
    //         test('endIndex is source.length - 1', () {
    //             final actual = base.endIndex;
    //             final expected = 2; 
    //             expect(actual, expected);
    //         });
    //     });
    // });

    // group('scan', () {
    //     group('expected: return T then exists element.', () {
    //         final elementA = MeaningfulLeafSyntax.fromType<LowercaseAlphabetA>(SyntaxPosition(1)).wrapped;
    //         final elementB = MeaningfulLeafSyntax.fromType<LowercaseAlphabetB>(SyntaxPosition(2)).wrapped;
    //         final elementC = MeaningfulLeafSyntax.fromType<LowercaseAlphabetC>(SyntaxPosition(3)).wrapped;
    //         final source = LeafSyntaxList([
    //             elementA,
    //             elementB,
    //             elementC,
    //         ]);
    //         final base = SyntaxScanner(source, <SecondLayerOrBelowSyntax>[]);
    //         test('first scan: return current element.', () {
    //             final actual = base.scan();
    //             final expected = elementA;
    //             expect(actual, expected);
    //         });
    //         test('second scan: return second element.', () {
    //             final actual = base.scan();
    //             final expected = elementB;
    //             expect(actual, expected);
    //         });
    //         test('over currentIndex: return null.', () {
    //             base.shift();
    //             final actual = base.scan();
    //             final expected = null;
    //             expect(actual, expected);
    //         });
    //     });
    //     test('expected: return null then source is empty', () {
    //         final base = SyntaxScanner(<LeafSyntax>[], <SecondLayerOrBelowSyntax>[]);
    //         final actual = base.scan();
    //         final expected = null;
    //         expect(actual, expected);
    //     });
    // });

    // group('look', () {
    //     final elementA = MeaningfulLeafSyntax.fromType<LowercaseAlphabetA>(SyntaxPosition(1)).wrapped;
    //     final elementB = MeaningfulLeafSyntax.fromType<LowercaseAlphabetB>(SyntaxPosition(2)).wrapped;
    //     final elementC = MeaningfulLeafSyntax.fromType<LowercaseAlphabetC>(SyntaxPosition(3)).wrapped;
    //     final source = LeafSyntaxList([
    //         elementA,
    //         elementB,
    //         elementC,
    //     ]);
    //     group('expected: return T then exists element.', () {
    //         final base = SyntaxScanner(source, <SecondLayerOrBelowSyntax>[]);
    //         test('first look: return current element.', () {
    //             final actual = base.look();
    //             final expected = elementA;
    //             expect(actual, expected);
    //         });
    //         test('second look: return current element.', () {
    //             final actual = base.look();
    //             final expected = elementA;
    //             expect(actual, expected);
    //         });
    //         test('over currentIndex: return null.', () {
    //             base.shift();
    //             base.shift();
    //             base.shift();
    //             final actual = base.look();
    //             final expected = null;
    //             expect(actual, expected);
    //         });
    //     });
    //     test('expected: return null then source is empty', () {
    //         final base = SyntaxScanner(<LeafSyntax>[], <SecondLayerOrBelowSyntax>[]);
    //         final actual = base.look();
    //         final expected = null;
    //         expect(actual, expected);
    //     });
    //     test('expected: return syntax then offset is two', () {
    //         final base = SyntaxScanner(source, <SecondLayerOrBelowSyntax>[]);
    //         final actual = base.look(2);
    //         final expected = elementC;
    //         expect(actual, expected);
    //     });
    //     test('expected: return null then lookIndex is negative', () {
    //         final base = SyntaxScanner(source, <SecondLayerOrBelowSyntax>[]);
    //         final actual = base.look(-1);
    //         final expected = null;
    //         expect(actual, expected);
    //     });
    //     test('expected: return null then lookIndex is over endIndex', () {
    //         final base = SyntaxScanner(source, <SecondLayerOrBelowSyntax>[]);
    //         final actual = base.look(3);
    //         final expected = null;
    //         expect(actual, expected);
    //     });
    // });

    // group('shift', () {
    //     final elementA = MeaningfulLeafSyntax.fromType<LowercaseAlphabetA>(SyntaxPosition(1)).wrapped;
    //     final elementB = MeaningfulLeafSyntax.fromType<LowercaseAlphabetB>(SyntaxPosition(2)).wrapped;
    //     final elementC = MeaningfulLeafSyntax.fromType<LowercaseAlphabetC>(SyntaxPosition(3)).wrapped;
    //     group('expected: move element to shift then exists element.', () {
    //         final source = LeafSyntaxList([
    //             elementA,
    //             elementB,
    //             elementC,
    //         ]);
    //         final base = SyntaxScanner(source, <SecondLayerOrBelowSyntax>[]);
    //         test('first shift: ', () {
    //             base.shift();
    //             final actual = base.currentIndex;
    //             final expected = 1;
    //             expect(actual, expected);
    //         });
    //         test('second shift: ', () {
    //             base.shift();
    //             final actual = base.currentIndex;
    //             final expected = 2;
    //             expect(actual, expected);
    //         });
    //         test('over shift: do nothing', () {
    //             base.shift();
    //             base.shift();
    //             final actual = base.currentIndex;
    //             final expected = 3;
    //             expect(actual, expected);
    //         });
    //     });
    //     test('expected: 指定された値だけ currentIndex に加算される.', () {
    //         final number = 2;
    //         final source = LeafSyntaxList([
    //             elementA,
    //             elementB,
    //             elementC,
    //         ]);
    //         final base = SyntaxScanner(source, <SecondLayerOrBelowSyntax>[]);
    //         base.shift(number);
    //         final actual = base.currentIndex;
    //         final expected = 2;
    //         expect(actual, expected);
    //     });
    //     test('expected: do nothing then source is empty', () {
    //         final base = SyntaxScanner(<LeafSyntax>[], <SecondLayerOrBelowSyntax>[]);
    //         base.shift();
    //         final actual = base;
    //         final expected = SyntaxScanner(<LeafSyntax>[], <SecondLayerOrBelowSyntax>[]);
    //         expect(actual, expected);
    //     });
    // });

}
