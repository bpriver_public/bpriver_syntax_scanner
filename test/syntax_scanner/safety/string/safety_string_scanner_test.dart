// Copyright (C) 2023, the bpriver_syntax_scanner project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_debug/bpriver_debug.dart';
import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:bpriver_syntax_scanner/bpriver_syntax_scanner.dart';
import 'package:test/test.dart';

void main() {

    // ignore: unused_local_variable
    final debug = BpriverDebug(testLocation: ['syntax_scanner', 'safety', 'string', 'safety_string_scanner']);

    final source = 'aabbcc';

    group('fromString', () {
        test('expected: return SafetyStringScanner then ', () {
            final base = SafetyStringScanner.fromString(source);
            final result = base;
            final actual = result;
            final expected = SafetyStringScanner(['a', 'a', 'b', 'b', 'c', 'c']);
            expect(actual, expected);
        });
    });
    
    group('lookWhileCharacters', () {
        test('expected: return List<String> then characters length is one.', () {
            final characters = [
                'a',
            ];
            final base = SafetyStringScanner<String>.fromString(source);
            final result = base.lookWhileCharacters(characters);
            final actual = result.wrapped;
            final expected = ['a', 'a'];
            expect(actual, expected);
            // lookIndex test
            {
                final actual = base.lookIndex;
                final expected = 2;
                expect(actual, expected);
            }
        });
        test('expected: return List<String> then characters length is two.', () {
            final characters = [
                'a',
                'b',
            ];
            final base = SafetyStringScanner<String>.fromString(source);
            final result = base.lookWhileCharacters(characters);
            final actual = result.wrapped;
            final expected = ['a', 'a', 'b', 'b'];
            expect(actual, expected);
            // lookIndex test
            {
                final actual = base.lookIndex;
                final expected = 4;
                expect(actual, expected);
            }
        });
        test('expected: return List<String> then characters is 当てはまらない.', () {
            final characters = [
                'x',
                'y',
            ];
            final base = SafetyStringScanner<String>.fromString(source);
            final result = base.lookWhileCharacters(characters);
            final actual = result.wrapped;
            final expected = <String>[];
            expect(actual, expected);
            // lookIndex test
            {
                final actual = base.lookIndex;
                final expected = 0;
                expect(actual, expected);
            }
        });
    });

    group('lookWhileUntils', () {
        test('expected: return List<String> then characters length is one.', () {
            final untils = [
                'b',
            ];
            final base = SafetyStringScanner<String>.fromString(source);
            final result = base.lookWhileUntils(untils);
            final actual = result.wrapped;
            final expected = ['a', 'a'];
            expect(actual, expected);
            // lookIndex test
            {
                final actual = base.lookIndex;
                final expected = 3;
                expect(actual, expected);
            }
        });
        test('expected: return List<String> then untils length is two.', () {
            final untils = [
                'c',
                'x',
            ];
            final base = SafetyStringScanner<String>.fromString(source);
            final result = base.lookWhileUntils(untils);
            final actual = result.wrapped;
            final expected = ['a', 'a', 'b', 'b'];
            expect(actual, expected);
            // lookIndex test
            {
                final actual = base.lookIndex;
                final expected = 5;
                expect(actual, expected);
            }
        });
        test('expected: return List<String> then untils is すぐに出現.', () {
            final untils = [
                'a',
            ];
            final base = SafetyStringScanner<String>.fromString(source);
            final result = base.lookWhileUntils(untils);
            final actual = result.wrapped;
            final expected = <String>[];
            expect(actual, expected);
            // lookIndex test
            {
                final actual = base.lookIndex;
                final expected = 1;
                expect(actual, expected);
            }
        });
        test('expected: return List<String> then 1文字の escape sequence', () {
            final source = 'aaxbbcc';
            final untils = [
                'b',
            ];
            final escapeSequence = 'x';
            final base = SafetyStringScanner<String>.fromString(source);
            final result = base.lookWhileUntils(untils, escapeSequence);
            final actual = result.wrapped;
            final expected = <String>['a', 'a', 'b'];
            expect(actual, expected);
            // lookIndex test
            {
                final actual = base.lookIndex;
                final expected = 5;
                expect(actual, expected);
            }
        });
        test('expected: return List<String> then 2文字以上の escape sequence', () {
            final source = 'aaxxxbbcc';
            final untils = [
                'b',
            ];
            final escapeSequence = 'xxx';
            final base = SafetyStringScanner<String>.fromString(source);
            final result = base.lookWhileUntils(untils, escapeSequence);
            final actual = result.wrapped;
            final expected = <String>['a', 'a', 'b'];
            expect(actual, expected);
            // lookIndex test
            {
                final actual = base.lookIndex;
                final expected = 7;
                expect(actual, expected);
            }
        });
        test('exception: return BpriverSyntaxScannerExceptionD then untils is 当てはまらない.', () {
            final untils = [
                'x',
                'y',
            ];
            final base = SafetyStringScanner<String>.fromString(source);
            final result = base.lookWhileUntils(untils);
            final actual = result.wrapped;
            final expected = BpriverSyntaxScannerExceptionD();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'list': ['a', 'a', 'b', 'b', 'c', 'c'],
                };
                expect(actual, expected);
            }
            // lookIndex test
            {
                final actual = base.lookIndex;
                final expected = 6;
                expect(actual, expected);
            }
        });
        test('error: throw BpriverSyntaxScannerErrorC then untils is 当てはまらない.', () {
            final untils = [
                'xxx',
                'x'
            ];
            final base = SafetyStringScanner<String>.fromString(source);

            try {
            
                base.lookWhileUntils(untils);

            } on Panic<BpriverSyntaxScannerErrorC> catch (result) {

                final actual = result.wrapped;
                final expected = BpriverSyntaxScannerErrorC();
                expect(actual, expected);
                {
                    final actual = result.log.getMonitor();
                    final expected = {
                        'error': 'xxx',
                        'untils': untils,
                    };
                    expect(actual, expected);
                }                            
            }
        });
    });

}
